import {
  EarthCanvas,
  BallCanvas,
  ComputersCanvas,
  StarsCanvas,
} from './canvas';
import Hero from './Hero';
import Navbar from './Navbar';
import Tech from './Tech';
import Experience from './Experience';
import Proyectos from './Proyectos';
import Feedbacks from './Feedbacks';
import Contact from './Contact';

export {
  Hero,
  Navbar,
  Tech,
  Experience,
  Proyectos,
  Feedbacks,
  Contact,
  EarthCanvas,
  BallCanvas,
  ComputersCanvas,
  StarsCanvas,
};

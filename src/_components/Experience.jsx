import React from 'react';
import {
  VerticalTimeline,
  VerticalTimelineElement,
} from 'react-vertical-timeline-component';
import { motion } from 'framer-motion';
import 'react-vertical-timeline-component/style.min.css';
import { experiences } from '@/_constants';
import { textVariant } from '@/_utils/motion';
import { SectionWrapper } from '@/_HOC';

const ExperienceCard = ({ experience }) => {
  return (
    <VerticalTimelineElement
      contentStyle={{ background: '#1d1836', color: '#fff' }}
      contentArrowStyle={{ borderRight: '7px solid #FFA500' }}
      iconStyle={{ background: experience.iconBg }}
    >
      <div>
        <h3 className="text-white text-[24px] semi-bold">{experience.title}</h3>
        <h5 className="semi-bold" style={{ color: '#99FF99' }}>
          {experience.company_name}
        </h5>
        <p className="text-secondary text-[13px]  " style={{ margin: 0 }}>
          {experience.points}
        </p>
      </div>
    </VerticalTimelineElement>
  );
};

const Experience = () => {
  return (
    <>
      <motion.div variants={textVariant()}>
        <h2 className="text-white font-black md:text-[30px] sm:text-[20px] xs:text-[20px] text-[30px]">
          Experiencia
        </h2>
      </motion.div>

      <div className="mt-20 flex flex-col">
        <VerticalTimeline>
          {experiences.map((experience, index) => (
            <ExperienceCard key={index} experience={experience} />
          ))}
        </VerticalTimeline>
      </div>
    </>
  );
};

export default SectionWrapper(Experience, 'experiencia');

import React from 'react';
import Image from 'next/image';
import { SectionWrapper } from '@/_HOC';
import { Tilt } from 'react-tilt';
import { motion } from 'framer-motion';
import { fadeIn, textVariant } from '@/_utils/motion';
import { projects } from '@/_constants';
import imgLogo from '../../public/assets/logo-white.png';

const ProjectCard = ({ index, name, description, image }) => {
  return (
    <motion.div variants={fadeIn('up', 'spring', index * 0.5, 0.75)}>
      <Tilt
        options={{
          max: 50,
          scale: 1,
          speed: 450,
        }}
        className="bg-gray  p-5 rounded-2xl sm:w-[300px] w-full"
      >
        <div className="relative w-full h-[230px] shadow-2xl">
          <Image
            src={image}
            alt={image}
            className="w-full h-full object-cover rounded-2xl"
          />
          <div className="absolute inset-0 flex justify-start m-3 card-img_hover">
            <div className="shadow-2xl yellow-gradient w-10 h-10 rounded-full flex justify-center items-center">
              <Image
                src={imgLogo}
                alt="logo"
                className="w-[100%] h-[100%] object-contain"
              />
            </div>
          </div>
        </div>

        <div className="mt-5 h-[150px]">
          <h3 className=" text-white font-bold text-[24px]">{name}</h3>
          <p className="mt2 text-secondary text-[14px]">{description}</p>
        </div>
      </Tilt>
    </motion.div>
  );
};

const Proyectos = () => {
  return (
    <>
      <motion.div variants={textVariant()}>
        <p className="sm:text-[12px] text-[10px] text-secondary uppercase tracking-wider">
          Nuestros Trabajos
        </p>
        <h2 className="text-white font-black md:text-[30px] sm:text-[20px] xs:text-[20px] text-[30px]">
          Proyectos
        </h2>
      </motion.div>
      <div className="w-full flex">
        <motion.p
          variants={fadeIn('', '', 0.1, 1)}
          className="w-full mt-3 text-secondary text-[14px]  leading-[20px]"
        >
          Lorem ipsum was conceived as filler text, formatted in a certain way
          to enable the presentation of graphic elements in documents, without
          the need for formal copy. Using Lorem Ipsum allows designers to put
          together layouts and the form of the content before the content has
          been created, giving the design and production process more freedom.
        </motion.p>
      </div>

      <div className="mt-5 flex flex-wrap gap-7  justify-between">
        {projects.map((project, index) => (
          <ProjectCard key={`project-${index}`} index={index} {...project} />
        ))}
      </div>
    </>
  );
};

export default SectionWrapper(Proyectos, 'proyectos');

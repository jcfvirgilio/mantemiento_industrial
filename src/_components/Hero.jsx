import React, { Suspense } from 'react';
import { services } from '@/_constants';

const Spline = React.lazy(() => import('@splinetool/react-spline'));

const ServiceCard = ({ title }) => {
  return (
    <>
      <div className="xs:w-[45%] w-full h-[10%] text-center background-gradient p-[4px] rounded-[5px] shadow-card cursor-pointer hover:text-secondary hover:border-white border-transparent border">
        <span className="relative flex h-3 w-3">
          <span className="animate-ping absolute inline-flex h-full w-full rounded-full bg-sky-400 opacity-75"></span>
          <span className="relative inline-flex rounded-full h-3 w-3 bg-sky-500"></span>
        </span>
        {title}
      </div>
    </>
  );
};

const Hero = () => {
  return (
    <section className="relative  w-full h-screen mx-auto flex-row ">
      <div className="absolute inset-0.5 top-[120px]  mx-auto sm:px-16 px-6  items-start gap-10  flex ">
        <div className="flex flex-col justify-center items-center mt-5">
          <div className="w-5 h-5 rounded-full bg-[#FFA500]" />
          <div className="w-1 sm:h-80 h-[390px]  orange-gradient" />
        </div>
        <div>
          <h1 className="sm:inline inline font-black text-white lg:text-[25px] sm:text-[18px] xs:text-[15px] text-[18px] lg:leading-[98px] mt-5">
            Mantenimiendo de Equipo&nbsp;
            <span className="text-[#99FF99]"> Industrial</span>
          </h1>
          <p className="text-[#dfd9ff] font-medium lg:text-[15px] sm:text-[18px] xs:text-[18px] text-[12px] lg:leading-[15px] mt-10 text-white-100">
            Estamos dedicados a proporcionar soluciones integrales de
            mantenimiento para tu equipo industrial.
            <br className="sm:block hidden" />
            Nos enorgullece ofrecer servicios confiables y profesionales que
            garantizan un rendimiento óptimo y una vida útil prolongada de tus
            equipos.
          </p>
          <div className="sm:mt-20 mt-10 flex flex-wrap gap-4 ">
            {services.map((service, index) => (
              <ServiceCard key={service.title} index={index} {...service} />
            ))}
          </div>
        </div>
        <Suspense fallback={<div>Loading...</div>}>
          <div className="mt-10 sm:h-[400px] w-full  sm:inline hidden ">
            <Spline scene="https://prod.spline.design/x8j9lv8FDIoY9BzI/scene.splinecode" />
          </div>
        </Suspense>
      </div>
    </section>
  );
};

export default Hero;

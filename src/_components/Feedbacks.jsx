import React from 'react';
import { motion } from 'framer-motion';
import { SectionWrapper } from '@/_HOC';
import Image from 'next/image';
import { testimonials } from '@/_constants';
import { fadeIn, textVariant } from '@/_utils/motion';
import imgLogo from '../../public/assets/logo-white.png';

const FeedbackCard = ({ index, testimonial, name, designation, company }) => (
  <motion.div
    variants={fadeIn('', 'spring', index * 0.5, 0.75)}
    className="bg-black-200 p-10 rounded-3xl xs:w-[300px] w-full"
  >
    <div className="inset-0 flex justify-start  ">
      <div className="shadow-2xl yellow-gradient w-10 h-10 rounded-full flex justify-center items-center">
        <Image
          src={imgLogo}
          alt="logo"
          className="w-[100%] h-[100%] object-contain"
        />
      </div>
    </div>
    <div className="mt-1">
      <p className="text-white tracking-wider text-[18px]"> {testimonial}</p>
      <div className="mt-7 flex justify-between items-center gap-1">
        <div className="flex-1 flex flex-col">
          <p className="text-white font-medium text-[16px]">
            <span className="blue-text-gradient">@</span> {name}
          </p>
          <p className="mt-1 text-scondary text-[12px]">
            {designation} de {company}
          </p>
        </div>
      </div>
    </div>
  </motion.div>
);

const Feedbacks = () => {
  return (
    <div className="mt-12 bg-black-100 rounded-[20px]">
      <div className="sm:px-16 px-6 sm:py-16 py-10 bg-tertiary rounded-2xl min-h-[300px]">
        <motion.div variants={textVariant()}>
          <p className="sm:text-[12px] text-[10px] text-secondary uppercase tracking-wider">
            Nuestros Clientes Dicen
          </p>
          <h2 className="text-white font-black md:text-[30px] sm:text-[20px] xs:text-[20px] text-[30px]">
            Testimonios
          </h2>
        </motion.div>
      </div>
      <div className="sm:px-16 px-6 -mt-40 pb-14 flex flex-wrap gap-7">
        {testimonials.map((testimonials, index) => (
          <FeedbackCard
            key={testimonials.name}
            index={index}
            {...testimonials}
          />
        ))}
      </div>
    </div>
  );
};

export default SectionWrapper(Feedbacks, '');

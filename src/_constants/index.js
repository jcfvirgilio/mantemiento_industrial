import {
  meta,
  starbucks,
  tesla,
  shopify,
  proyecto_1,
  proyecto_2,
  proyecto_3,
} from '../../public/assets';

export const navLinks = [
  {
    id: 'experiencia',
    title: 'Experiencia...',
  },
  {
    id: 'proyectos',
    title: 'Proyectos',
  },
  {
    id: 'contacto',
    title: 'Contacto',
  },
];

const services = [
  {
    title: 'Renovación Industrial',
    src: 'https://prod.spline.design/x8j9lv8FDIoY9BzI/scene.splinecode',
  },
  {
    title: 'Mantenimiento Predictivo',
    src: 'https://prod.spline.design/Ub64u6XO-yVUowCi/scene.splinecode',
  },
  {
    title: 'Mantenimiento Correctivo',
    src: 'https://prod.spline.design/idWxwxfN34y5qF7O/scene.splinecode',
  },
  {
    title: 'Recuperación y Restauración de Equipos Industriales',
    src: 'https://prod.spline.design/eT3w-JiMWssVD6gG/scene.splinecode',
  },
];

const experiences = [
  {
    title: 'Fresadora',
    company_name: 'Madera, Acero, Hierro',
    icon: starbucks,
    iconBg: '#383E56',
    date: 'March 2020 - April 2021',
    points: [
      'Developing and maintaining web applications using React.js and other related technologies.',
      'Collaborating with cross-functional teams including designers, product managers, and other developers to create high-quality products.',
      'Implementing responsive design and ensuring cross-browser compatibility.',
      'Participating in code reviews and providing constructive feedback to other developers.',
    ],
  },
  {
    title: 'Guillotinas',
    company_name: 'Cartón, Madera,Materias Plásticas',
    icon: tesla,
    iconBg: '#E6DEDD',
    date: 'Jan 2021 - Feb 2022',
    points: [
      'Developing and maintaining web applications using React.js and other related technologies.',
      'Collaborating with cross-functional teams including designers, product managers, and other developers to create high-quality products.',
      'Implementing responsive design and ensuring cross-browser compatibility.',
      'Participating in code reviews and providing constructive feedback to other developers.',
    ],
  },
  {
    title: 'Troqueladoras',
    company_name: 'Hidráulica',
    icon: shopify,
    iconBg: '#383E56',
    date: 'Jan 2022 - Jan 2023',
    points: [
      'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting',
    ],
  },
  {
    title: 'Dobladoras',
    company_name: '3 o 4 Rodillos',
    icon: meta,
    iconBg: '#E6DEDD',
    date: 'Jan 2023 - Present',
    points: [
      'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.',
    ],
  },
];

const testimonials = [
  {
    testimonial:
      'I thought it was impossible to make a website as beautiful as our product, but Rick proved me wrong.',
    name: 'Sara Lee',
    designation: 'CFO',
    company: 'Acme Co',
    image: 'https://randomuser.me/api/portraits/women/4.jpg',
  },
  {
    testimonial:
      'I never met a web developer who truly cares about their clients success like Rick does.',
    name: 'Chris Brown',
    designation: 'COO',
    company: 'DEF Corp',
    image: 'https://randomuser.me/api/portraits/men/5.jpg',
  },
  {
    testimonial:
      'After Rick optimized our website, our traffic increased by 50%. We can not thank them enough!',
    name: 'Lisa Wang',
    designation: 'CTO',
    company: '456 Enterprises',
    image: 'https://randomuser.me/api/portraits/women/6.jpg',
  },
];

const projects = [
  {
    name: 'Proyecto 1',
    description:
      'qui dolorem ipsum, quia dolor sit amet consectetur adipisci velit, sed quia non numquam eius modi tempora incidunt, ut labore et dolore magnam aliquam quaerat voluptatem',

    image: proyecto_1,
  },
  {
    name: 'Proyecto IT',
    description:
      'Lorem Ipsum was reintroduced in the 1980s by the Aldus Corporation, a company that developed Desktop Publishing Software. Their most well known product',

    image: proyecto_2,
  },
  {
    name: 'Proyecto Guide',
    description:
      'It is widely believed that the history of Lorem Ipsum originates with Cicero in the 1st Century BC and his text De Finibus bonorum et malorum.',
    image: proyecto_3,
  },
];

export { services, experiences, testimonials, projects };

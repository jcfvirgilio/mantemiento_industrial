import './globals.css';

export const metadata = {
  title: 'Mantenimiendo de Equipo Industrial',
  description: 'Mantenimiendo de Equipo Industrial',
};

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <body>
        {children}
      </body>
    </html>
  );
}
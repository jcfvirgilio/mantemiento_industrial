'use client';
import {
  Experience,
  Hero,
  Navbar,
  Proyectos,
  Feedbacks,
  Contact,
  StarsCanvas,
} from '@/_components';

export default function Home() {
  return (
    <div className="relative z-0 bg-primary">
      <div className="bg-hero-pattern bg-cover bg-no-repeat bg-center">
        <Navbar />
        <Hero />
      </div>
      <Experience />
      <Proyectos />
      <Feedbacks />
      <div className="relative z-0">
        <Contact />
        <StarsCanvas />
      </div>
    </div>
  );
}

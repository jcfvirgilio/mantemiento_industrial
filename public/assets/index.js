import logo from './logo.svg';
import menu from './menu.svg';
import close from './close.svg';

import proyecto_1 from './proyecto_1.png';
import proyecto_2 from './proyecto_2.jpg';
import proyecto_3 from './proyecto_3.jpg';

export { logo, menu, close, proyecto_1, proyecto_2, proyecto_3 };
